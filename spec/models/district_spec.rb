require 'rails_helper'

RSpec.describe District, :type => :model do

  subject(:district) { FactoryGirl.create(:district) }

  it "has a valid factory" do
    expect(district).to be_valid
  end

  it { is_expected.to validate_presence_of(:name)}
  it { is_expected.to validate_presence_of(:number)}
  it { is_expected.to have_many(:occurrences)}

  describe ".get_city" do
    it "get the city of São Paulo" do
      city = District.get_city
      expect(city.name).to eql 'São Paulo'
      expect(city.number).to be 0
    end
  end

  describe ".get_district" do
    context "when district exists in database" do
      let(:text) { "#{district.number} DP - #{district.name}" }

      it "is equal" do
        d = District.get_district text
        expect(district.id).to eql d.id
      end
    end

    context "when district doesn't exists in database" do
      let(:text) { "#{district.number+1} DP - #{district.name}2" }

      it "create new" do
        new_district = District.get_district text
        expect(new_district.id).to_not eql district.id
      end
    end
  end
end