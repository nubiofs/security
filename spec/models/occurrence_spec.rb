RSpec.describe Occurrence, :type => :model do

  subject(:occurrence) { FactoryGirl.create(:occurrence, :dp) }

  it "has a valid factory" do
    expect(occurrence).to be_valid
  end

  it { is_expected.to validate_presence_of(:crime_type_id)}
  it { is_expected.to validate_presence_of(:year)}
  it { is_expected.to validate_presence_of(:month)}
  it { is_expected.to validate_presence_of(:value)}
  it { is_expected.to validate_presence_of(:district_id)}
  it { is_expected.to validate_uniqueness_of(:district_id).scoped_to([:crime_type_id, :year, :month])}
  it { should belong_to(:crime_type) }
  it { should belong_to(:district) }

  describe ".city_data" do
    let(:occurrence_city) { FactoryGirl.create(:occurrence, :city) }
    it "should return a occurrence" do
      data = Occurrence.city_data occurrence_city.crime_type.id.to_s, occurrence_city.month.to_s, occurrence_city.year.to_s
      expect(data).to_not be_empty
    end
  end

end