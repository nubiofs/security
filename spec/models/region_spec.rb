RSpec.describe Region, :type => :model do

  subject(:region) { FactoryGirl.create(:region) }

  it "has a valid factory" do
    expect(region).to be_valid
  end

  it { is_expected.to validate_presence_of(:name)}
  it { is_expected.to validate_presence_of(:polygon)}
  it { is_expected.to validate_uniqueness_of(:name)}
  it { should belong_to(:district) }

end