require 'rails_helper'


RSpec.describe BrowserFactory, :type => :model do

  before(:all) do
    @headless = Headless.new
    @headless.start
  end

  describe ".create_browser" do
    it "create a browser with public download_path" do
      browser = BrowserFactory.create_browser
      expect(browser).to respond_to :download_path
      expect(browser.title).to eql 'Secretaria de Estado da Segurança Pública - Governo do Estado de São Paulo'
      expect(browser.text).to include 'Dados estatísticos do Estado de São Paulo'
      expect(browser.select_list(id: 'ContentPlaceHolder1_ddlRegioes').selected_options[0].text).to eql 'Capital'
      expect(browser.select_list(id: 'ContentPlaceHolder1_ddlMunicipios').selected_options[0].text).to eql 'São Paulo'
      browser.close
    end
  end 

  after(:all) do
    @headless.destroy
  end
end