require 'rails_helper'

RSpec.describe KmlBuilder, :type => :model do
  
  describe '.build' do
    let(:region) { FactoryGirl.create(:region) }
    let(:hash) {{region.name => 1000}}
    let(:params) {{:select_month => 1}}
    it 'receive a hash and parameters' do
      kml = KmlBuilder.build hash, params
      expect(kml).not_to be_empty
      # expect(kml).to include(region.name) skip for now, using district name instead
      expect(kml).to include(region.polygon)      
      doc = Nokogiri::XML(kml).remove_namespaces!
      expect(doc.text).not_to be_empty
    end
  end
end
