require 'rails_helper'

RSpec.describe DownloadCSV, :type => :model do
  before(:all) do
    @headless = Headless.new
    @headless.start
    @browser = BrowserFactory.create_browser
  end

  describe '.set_district' do
    it 'select the city' do
      text = DownloadCSV.set_district @browser, -1
      district = District.get_city
      expect(district.name).to eql text
      expect(@browser.select_list(id: 'ContentPlaceHolder1_ddlDelegacias').selected_options[0].text).to eql 'Todos'
    end

    it 'select a district' do
      text = DownloadCSV.set_district @browser, 0
      district = District.get_district(text)
      expect(district).to_not be_nil
      expect(district).to be_a District
      expect(@browser.select_list(id: 'ContentPlaceHolder1_ddlDelegacias').selected_options[0].text).to eql text
    end
  end

  describe '.DownloadCSV' do
    it 'download the files' do
      FileUtils.rm_rf(Dir.glob(@browser.download_path))
      DownloadCSV.download_csv @browser
      sleep(5)
      files_count = Dir[File.join(@browser.download_path, '**', '*')].count { |file| File.file?(file) }
      expect(files_count).to be 2
    end
  end

  after(:all) do
    @browser.close
    @headless.destroy
  end
end
