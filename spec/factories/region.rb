FactoryGirl.define do

  factory :region do
    name "REGION"
    polygon "SOME COORDINATES"
    district
  end
end