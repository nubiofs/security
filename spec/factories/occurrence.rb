FactoryGirl.define do

  factory :occurrence do
    year 2015
    month 4
    value 86

    trait :dp do
      district
    end

    trait :city do
      district {District.find_by(number: 0)}
    end

    before(:create) do |oc|
      oc.crime_type = CrimeType.find_or_create_by!(name: FactoryGirl.build(:crime_type).name)
    end
  end
end