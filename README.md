![Build Status](https://gitlab.com/smart-city-platform/security/badges/master/build.svg)

# Introduction
This application aims to create a service in which people can find criminal
occurrences in their districts.

The current development allows the user to filter occurrences and display
statistics on a chart and a map.

# Requirements
- Ruby 2.3
- Rails 4.2.6
- XVFB
- PostgreSQL
- Firefox 45 or below

# Setup

## Firefox (if needed)
```
sudo apt-get purge firefox
sudo apt-get install firefox=45.0.2+build1-0ubuntu1
```

## XVFB
```
sudo apt-get install xvfb
```

## PostgreSQL
```
sudo apt-get update
sudo apt-get install postgresql postgresql-contrib libpq-dev
sudo -u postgres createuser -rds <your-linux-username>
sudo -u postgres createuser -rds pguser
sudo -u postgres psql
\password pguser
<password>
\q
```
Note: Redeploy the database using `rake db:setup` after installing PostgreSQL

# Database

All information from the Database is directly extracted from the Secretaria de 
Estado da Segurança Pública website, located here: 
http://www.ssp.sp.gov.br/novaestatistica/Pesquisa.aspx

# Instructions
To import criminal data:
```
rake crawler:run
```
For test runs, only data from 2016 was imported, but it succesfully acquired from all 93 districts
of São Paulo. Individual database populating lasted for up to 15 minutes.

The suggested settings for testings are: 0..5 Districts, Year 2016.

It's possible to schedule crawler runs in Unix-like operating systems (It schedules the crawler to run every Sunday at 4:00 AM):
```
whenever --update-crontab
```

The project use kml/kmz files to show map overlays. Currently these files are stored on linux.ime.usp.br and you must provide valid credentials to upload the overlay files on linux.ime.usp.br. Example: 
```
SCP_REMOTE_LOGIN=<login> SCP_REMOTE_PASSWORD=<password> rails s
```
This was done because Google Maps requires public URLs to work with.

To see criminal statistics, click on the "Charts" link, and for a map of occurrences, click on the "Map" link.