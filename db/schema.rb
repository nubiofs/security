# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160605191745) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "crime_types", force: :cascade do |t|
    t.string "name"
  end

  create_table "districts", force: :cascade do |t|
    t.string  "name"
    t.integer "number"
  end

  create_table "occurrences", force: :cascade do |t|
    t.integer "district_id"
    t.integer "year"
    t.integer "month"
    t.integer "crime_type_id"
    t.integer "value"
  end

  add_index "occurrences", ["district_id", "crime_type_id", "year", "month"], name: "occurrence_unique_index", unique: true, using: :btree

  create_table "regions", force: :cascade do |t|
    t.string  "name"
    t.text    "polygon"
    t.integer "district_id"
  end

end
