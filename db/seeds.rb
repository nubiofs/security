# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
districts = [
  {:name => "São Paulo", :number => 0}
]

districts.each do |district|
  District.create!(district)
end

# parse locations from kml file
file = File.open(Rails.root.join('data/').join('doc.kml').to_s)
doc = Nokogiri::XML(file).remove_namespaces!
doc.xpath("//Placemark").each{ |node|
  name = node.xpath(".//SimpleData[@name = 'nm_distrit']").first.text
  polygon = node.xpath(".//coordinates").text.gsub(/\s+/, ' ')
  Region.create!(name: name, polygon: polygon)
}

