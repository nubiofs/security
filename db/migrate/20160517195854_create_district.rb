class CreateDistrict < ActiveRecord::Migration
  def change
    create_table :districts do |t|
      t.string :name, :unique => true
      t.integer :number, :unique => true
    end
  end
end
