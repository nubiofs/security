class CreateRegion < ActiveRecord::Migration
  def change
    create_table :regions do |t|
      t.string :name, :unique => true
      t.text :polygon
      t.references :district
    end
  end
end
