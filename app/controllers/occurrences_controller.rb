class OccurrencesController < ApplicationController
  include UploadHelper
  include KmlHelper

  def index
  end

  def charts
    setup_dropdowns
    @chart = ChartFactory.get_chart params[:select_district], params[:select_crime_type], params[:select_month], params[:select_year]
  end

  def maps
    setup_dropdowns
    create_overlay
  end

private
    def setup_dropdowns
      @district_list = District.order(:name)
      @crime_type_list = CrimeType.order(:name)
      @month_list = (1..12).map {|m| [Date::MONTHNAMES[m], m]}
      @year_list = (Occurrence.minimum(:year)..Occurrence.maximum(:year)).map {|m| [m, m]}
    end

    def create_overlay
      # setting filter with the params
      filter = Occurrence.map_filter params[:select_year], params[:select_month], params[:select_crime_type]        

      # Create kml and save to a tmp file
      text = KmlBuilder.build filter, params
      file_name = Rails.root.join("kml_tmp").join("year_#{params[:select_year]}_month_#{params[:select_month]}_crime_type_#{params[:select_crime_type]}").to_s
      save_kml text, file_name

      # zip kml and rename to kmz
      zip_kml file_name

      #upload file
      upload_file("#{file_name}.kmz")
      @kmz_url = get_remote_url("#{file_name}.kmz")
    end 
end