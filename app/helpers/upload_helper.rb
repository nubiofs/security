module UploadHelper

  HOST = 'linux.ime.usp.br'
  LOGIN = ENV['SCP_REMOTE_LOGIN']
  PASSWORD = ENV['SCP_REMOTE_PASSWORD']
  FOLDER_TO = "/home/bcc/#{LOGIN}/www"

  def upload_file full_filename
    puts FOLDER_TO
    Net::SCP.start(HOST, LOGIN, :password => PASSWORD) do |scp|
      scp.upload(full_filename, FOLDER_TO)
    end
  end

  def get_remote_url full_filename
    "http://#{HOST}/~#{LOGIN}/#{File.basename(full_filename)}"
  end

end