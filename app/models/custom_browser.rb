require 'watir'
require 'watir-webdriver'

class CustomBrowser

  attr_reader :download_path

  def initialize(type, download_path, options)
    @download_path = download_path
    FileUtils.mkdir_p download_path
    @browser = Watir::Browser.new(type, options)
  end

  #CustomBrowser qaucks like a Watir::Browser
  def method_missing(m, *args, &block)
    @browser.send(m, *args, &block)
  end

  def respond_to?(method, include_private = false)
    super || @browser.respond_to?(method, include_private)
  end

end