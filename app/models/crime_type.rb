class CrimeType < ActiveRecord::Base
  validates :name, uniqueness: true, presence: true
  has_many :occurrences
  
  def self.get_crime_type name
    CrimeType.find_by(name: name) || CrimeType.create(name: name)
  end
end