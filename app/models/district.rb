require 'csv'
class District < ActiveRecord::Base

  validates :name, uniqueness: true, presence: true
  validates :number, uniqueness: true, presence: true

  has_many :regions
  has_many :occurrences

  def self.get_city
    find_by(number: 0)
  end

  def self.get_district text
    m = text.match /(\d+) DP - (.+)/
    unless m.nil?
      num = m[1].to_i
      name = m[2]
      district = District.find_by(number: num)
      if district.nil?
        district = District.create(number: num, name: name)
        link_regions district        
      end
      district
    end
  end
  
private
    def self.link_regions district
      data = CSV.read(Rails.root.join('data/').join('dp2reg.csv').to_s, {:col_sep => ';'})
      data.each do |tuple|
        if (tuple[0].to_i == district.number)
          region = Region.find_by(name: Region.format_name(tuple[1]))
          if !region.nil? and region.district.nil?
            region.district = district
            region.save
          end
        end
      end
    end
end