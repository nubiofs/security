require 'watir'
require 'watir-webdriver'

class BrowserFactory

  @@DL_DIR = Rails.root.join('download/').to_s
  @@count = 0

  def self.create_browser
    download_dir = @@DL_DIR + (@@count).to_s + '/'
    @@count = @@count + 1    
    browser = CustomBrowser.new(:firefox, download_dir, :profile => get_download_profile(download_dir))
    prepare_page browser
    browser
  end

  def self.get_download_profile download_dir
    profile = Selenium::WebDriver::Firefox::Profile.new
    profile['browser.download.lastDir'] = download_dir
    profile['browser.download.folderList'] = 2
    profile['browser.download.dir'] = download_dir
    profile['browser.download.manager.showWhenStarting'] = false
    profile['browser.helperApps.alwaysAsk.force'] = false
    profile['browser.helperApps.neverAsk.openFile'] = "text/csv,application/pdf"
    profile['browser.helperApps.neverAsk.saveToDisk'] = "text/csv,application/pdf,text/plain,application/text"
    profile['pdfjs.disabled'] = true
    profile
  end

  def self.prepare_page browser
    browser.goto 'http://www.ssp.sp.gov.br/novaestatistica/Pesquisa.aspx'
    browser.select_list(:id => 'ContentPlaceHolder1_ddlRegioes').select 'Capital'
    browser.select_list(:id => 'ContentPlaceHolder1_ddlMunicipios').select 'São Paulo'
  end

end