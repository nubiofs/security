class Occurrence < ActiveRecord::Base
  belongs_to :district
  belongs_to :crime_type  

  validates :crime_type_id, presence: true
  validates :year, presence: true
  validates :month, presence: true
  validates :value, presence: true
  validates :district_id, presence: true, uniqueness: {scope: [:crime_type_id, :year, :month]}

  def self.completed? district, year
    where('district_id = ?', district.id).where('year = ?', year).distinct.count(:month) == 12
  end

  def self.map_filter year, month, crime_type
    filter_year(year)
      .filter_month(month)
      .filter_crime_type(crime_type)
      .joins(district: :regions)
      .select("regions.name")
      .group("regions.name")
      .sum(:value)
  end

  def self.city_data crime, month, year
    filter_year(year).
      filter_month(month).
      filter_crime_type(crime).
      filter_district(District.get_city.id.to_s).
      group(:month).group(:year).order(:year).order(:month).sum(:value)
  end

  def self.optional_filter(*args)
    args.each do |arg|
      self.class_eval("scope :filter_#{arg}, ->(#{arg}) { (#{arg}.nil? or #{arg}.empty?)? (all): (where(#{arg}: #{arg})) }")
    end
  end  
  optional_filter :year, :month, :crime_type, :district
end