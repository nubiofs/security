require 'watir'
require 'watir-webdriver'

class DownloadCSV

  def self.set_district browser, i
    if i == -1
      browser.select_list(:id => 'ContentPlaceHolder1_ddlDelegacias').select 'Todos'
      'São Paulo'
    else
      browser.link(:id => "ContentPlaceHolder1_repLateral_lkLateral_#{i}").click
      browser.link(:id => "ContentPlaceHolder1_repLateral_lkLateral_#{i}").text
    end
  end

  # Download the .csv files
  def self.download_csv browser, year=Time.now.year
    browser.select_list(:id => 'ContentPlaceHolder1_ddlAnos').select year
    browser.link(:id => 'ContentPlaceHolder1_btnPolicial').click
    browser.button(:id => 'ContentPlaceHolder1_btnExcel').click
    browser.link(:id => 'ContentPlaceHolder1_btnMensal').click
    browser.button(:id => 'ContentPlaceHolder1_btnExcel').click
    
  end
end





