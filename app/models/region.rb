class Region < ActiveRecord::Base

  before_save self
  validates :name, uniqueness: true, presence: true
  validates :polygon, presence: true
  belongs_to :district

  def before_save record
    record.name = self.class.format_name(record.name)
  end

  def self.format_name name
    I18n.transliterate(name).upcase
  end
end