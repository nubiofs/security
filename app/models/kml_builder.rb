class KmlBuilder

  @@LINE_COLOR = "ff000000"
  @@POLY_COLORS = ["88ebf7ff", "8899d6ff", "884db7ff", "884490f2", "883b69e5", "883643f4", "882f2fd3", "882424a8", "88191976"]

  def self.build regions, params
    result = ""
    result += KmlBuilder.create_kml regions, params
  end

private  
    def self.create_kml regions, params
      temp = '<?xml version="1.0" encoding="UTF-8"?><kml xmlns="http://www.opengis.net/kml/2.2" xmlns:gx="http://www.google.com/kml/ext/2.2" xmlns:kml="http://www.opengis.net/kml/2.2" xmlns:atom="http://www.w3.org/2005/Atom"><Document id="root_doc"><name>distrito_municipal.kml</name><open>1</open>'
      temp += KmlBuilder.create_styles
      temp += KmlBuilder.create_folder regions, params
      temp += '</Document></kml>'
    end

    def self.create_styles
      temp = ''
      id = 0
      @@POLY_COLORS.each{ |poly_color|
        temp += KmlBuilder.create_style id, @@LINE_COLOR, poly_color
        id += 1
      }
      temp
    end

    def self.create_style id, line_color, poly_color
      '<Style id="style_' + id.to_s + '"><LineStyle><color>' + line_color + '</color></LineStyle><PolyStyle><color>' + poly_color + '</color></PolyStyle></Style>'
    end

    def self.create_folder regions, params
      temp = '<Folder><name>distrito_municipal</name><open>1</open>'
      temp += KmlBuilder.create_regions regions, params
      temp += '</Folder>'
      temp
    end

    def self.create_regions regions, params
      temp = ''
      max = @@POLY_COLORS.size-1;
      regions.each_value{ |value|
        max = value if value > max
      }

      regions.each{ |name, value|
        temp += KmlBuilder.create_region name, value, "#style_#{((@@POLY_COLORS.size-1)*value.to_f/max).to_i}", params
      }
      temp
    end

    def self.create_region name, value, style, params
      district = Region.find_by_name(name).district
      url = ActionController::Base.helpers.link_to("Visualizar gráfico", Rails.application.routes.url_helpers.occurrences_charts_path(
        :select_district => district.id,
        :select_crime_type => params[:select_crime_type], 
        :select_month => params[:select_month], 
        :select_year => params[:select_year])).encode(:xml => :text)

      temp = "<Placemark><name>#{district.name}(#{value} ocorrências)</name><description>#{url}</description><styleUrl>#{style}</styleUrl><MultiGeometry><Polygon><altitudeMode>relativeToGround</altitudeMode><outerBoundaryIs><LinearRing><altitudeMode>relativeToGround</altitudeMode><coordinates>"
      temp += KmlBuilder.create_coordinates name
      temp += '</coordinates></LinearRing></outerBoundaryIs></Polygon></MultiGeometry></Placemark>'
    end

    def self.create_coordinates name
      Region.find_by(name: name).polygon
    end
end