require_relative 'chart'

class LineChart < Chart

  def initialize district, crime, month, year
    super(district, crime, month, year)
    @type = :line
    
    filter = Occurrence
      .filter_year(year)
      .filter_month(month)
      .filter_district(district)
      .filter_crime_type(crime)

    tempData = filter
      .group(:month)
      .group(:year)
      .order(:year)
      .order(:month)
      .sum(:value)    
    d_data = Hash.new
    tempData.map do |k, v| 
      d_data[Date::MONTHNAMES[k[0]]+"/"+k[1].to_s] = v
    end
    city_data = Occurrence.city_data crime, month, year
    avg_data = Hash.new
    city_data.map do |k, v| 
      avg_data[Date::MONTHNAMES[k[0]]+"/"+k[1].to_s] = v/93
    end
    @data = [
      {name: District.find(district).name, data: d_data},
      {name: "Average", data: avg_data}]
  end
end