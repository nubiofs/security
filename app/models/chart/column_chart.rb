require_relative 'chart'

class ColumnChart < Chart

  def initialize district, crime, month, year
    super(district, crime, month, year)
    @type = :column
    
    filter = Occurrence
      .filter_year(year)
      .filter_month(month)
      .filter_district(district)
      .filter_crime_type(crime)

    tempData = filter
      .group(:month)
      .group(:year)
      .order(:year)
      .order(:month)
      .sum(:value)
    @data = Hash.new
    tempData.map do |k, v| 
      @data[District.find(district).name] = v
    end
    city_data = Occurrence.city_data crime, month, year
    avg_data = Hash.new
    city_data.map do |k, v| 
      avg_data["Average"] = v/93
    end
    @data.merge! avg_data
  end
end