require_relative 'chart'

class PieChart < Chart

  def initialize district, month, year
    super(district, '', month, year)
    @type = :pie
    
    @data = Occurrence
      .filter_year(year)
      .filter_month(month)
      .filter_district(district)
      .joins(:crime_type)
      .group(:name)
      .sum(:value)
  end
end