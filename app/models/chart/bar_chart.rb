require_relative 'chart'

class BarChart < Chart

  def initialize crime, month, year
    super('', crime, month, year)
    @type = :bar

    @data = Occurrence
      .filter_year(year)
      .filter_month(month)
      .filter_crime_type(crime)
      .joins(:district)
      .merge(District.order(:name))
      .group(:name)
      .sum(:value)
    @data.delete("São Paulo")
  end
end